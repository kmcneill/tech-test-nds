package com.mcn.diagnostic;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mcn.diagnostic.model.CholesterolLevel;
import com.mcn.diagnostic.model.DiagnosticReport;
import com.mcn.diagnostic.model.Patient;

public class DiagnosticReportCreatorTest {

	private DiagnosticReportCreator drc;
	
	@Before
	public void testReadJsonFromFile() throws JsonParseException, JsonMappingException, IOException {
		InputStream jsonFile = this.getClass().getClassLoader().getResourceAsStream("patientdetails.json");

		drc = new DiagnosticReportCreator(jsonFile);

		assertEquals(5, drc.patients.size());
	}

	@Test
	public void testCreateDiagnosticReport_ChildNormal() {
		testCreateDiagnosticReport(165, 17, CholesterolLevel.NORMAL);
	}

	@Test
	public void testCreateDiagnosticReport_ChildBorderline() {
		testCreateDiagnosticReport(199, 17, CholesterolLevel.BORDERLINE);
	}

	@Test
	public void testCreateDiagnosticReport_ChildHigh() {
		testCreateDiagnosticReport(200, 17, CholesterolLevel.HIGH);
	}

	@Test
	public void testCreateDiagnosticReport_AdultNormal() {
		testCreateDiagnosticReport(199, 21, CholesterolLevel.NORMAL);
	}

	@Test
	public void testCreateDiagnosticReport_AdultBorderline() {
		testCreateDiagnosticReport(239, 21, CholesterolLevel.BORDERLINE);
	}

	@Test
	public void testCreateDiagnosticReport_AdultHigh() {
		testCreateDiagnosticReport(240, 21, CholesterolLevel.HIGH);
	}

	private void testCreateDiagnosticReport(int bloodCholesterolReading, int age,
			CholesterolLevel expectedCholesterolLevel) {

		Patient patient = new Patient("p1", "John", "Doe", LocalDate.now().minusYears(age),
				bloodCholesterolReading);

		DiagnosticReport report = drc.createDiagnosticReport(patient, age);

		assertThat(report, equalTo(expectedReport(patient, age, expectedCholesterolLevel)));
	}

	@Test
	public void testCreateAge() {
		LocalDate dob = LocalDate.parse("1986-03-12");

		assertEquals(34, drc.getAge(LocalDate.parse("2021-03-11"), dob));
		assertEquals(35, drc.getAge(LocalDate.parse("2021-03-12"), dob));
	}

	private DiagnosticReport expectedReport(Patient p, int age, CholesterolLevel c) {
		DiagnosticReport report = new DiagnosticReport(p);
		report.setAge(age);
		report.setCholesterolLevel(c);
		return report;
	}
	

}
