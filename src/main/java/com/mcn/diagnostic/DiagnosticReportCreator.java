package com.mcn.diagnostic;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcn.diagnostic.model.CholesterolLevel;
import com.mcn.diagnostic.model.DiagnosticReport;
import com.mcn.diagnostic.model.Patient;
import com.mcn.diagnostic.model.PatientList;

public class DiagnosticReportCreator {

	private static final int CHILD_BORDERLINE_LIMIT = 170;
	private static final int CHILD_HIGH_LIMIT = 200;
	private static final int ADULT_BORDERLINE_LIMIT = 200;
	private static final int ADULT_HIGH_LIMIT = 240;
	
	private final ObjectMapper mapper = new ObjectMapper();

	protected Map<String, Patient> patients;

	public DiagnosticReportCreator(InputStream jsonFileStream)
			throws JsonParseException, JsonMappingException, IOException {
		patients = createPatientListFromFile(jsonFileStream);
	}

	private Map<String, Patient> createPatientListFromFile(InputStream jsonFileStream)
			throws JsonParseException, JsonMappingException, IOException {
		PatientList patientList = this.parseJsonFromFile(jsonFileStream);
		return patientList.getPatients().stream().collect(Collectors.toMap(Patient::getId, patient -> patient));
	}

	private PatientList parseJsonFromFile(InputStream jsonFileStream)
			throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(jsonFileStream, new TypeReference<PatientList>() {
		});
	}

	public DiagnosticReport createDiagnosticReport(String id) {
		if (!patients.containsKey(id)) {
			throw new IllegalArgumentException("No Patient found for ID: ".concat(id));
		}

		Patient patient = patients.get(id);

		int age = getAge(LocalDate.now(), patient.getBirthDate());

		return this.createDiagnosticReport(patient, age);
	}

	protected DiagnosticReport createDiagnosticReport(Patient patient, int age) {
		CholesterolLevel level = getCholesterolLevel(age, patient.getBloodCholesterolReading());
		DiagnosticReport report = new DiagnosticReport(patient);
		report.setAge(age);
		report.setCholesterolLevel(level);
		return report;
	}

	protected int getAge(LocalDate now, LocalDate dob) {
		int age = now.getYear() - dob.getYear();
		LocalDate birthday = LocalDate.of(now.getYear(), dob.getMonth(), dob.getDayOfMonth());
		if (birthday.isAfter(now)) {
			age--;
		}
		return age;
	}

	private CholesterolLevel getCholesterolLevel(int age, int bloodCholesterolReading) {
		if (age < 18) {
			return calculateCholesterolLevel(bloodCholesterolReading, CHILD_BORDERLINE_LIMIT, CHILD_HIGH_LIMIT);
		}
		return calculateCholesterolLevel(bloodCholesterolReading, ADULT_BORDERLINE_LIMIT, ADULT_HIGH_LIMIT);
	}

	private CholesterolLevel calculateCholesterolLevel(int bloodCholesterolReading, int borderlineLimit,
			int highLimit) {
		if (bloodCholesterolReading < borderlineLimit) {
			return CholesterolLevel.NORMAL;
		}
		if (bloodCholesterolReading < highLimit) {
			return CholesterolLevel.BORDERLINE;
		}
		return CholesterolLevel.HIGH;
	}
}
