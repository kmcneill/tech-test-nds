package com.mcn.diagnostic.model;

public enum CholesterolLevel {

	NORMAL, BORDERLINE, HIGH;

}
