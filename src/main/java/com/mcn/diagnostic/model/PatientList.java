package com.mcn.diagnostic.model;

import java.util.List;

import lombok.Data;

@Data
public class PatientList {

	private List<Patient> patients;

}
