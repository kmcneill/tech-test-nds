package com.mcn.diagnostic.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DiagnosticReport {
	
	private String id;
	
	private String name;
	
	private int age;
	
	private int cholesterolReading;
	
	private CholesterolLevel cholesterolLevel;
	
	public DiagnosticReport(Patient patient) {
		this.setId(patient.getId());
		this.setName(patient.getFirstName().concat(" ").concat(patient.getLastName()));
		this.setCholesterolReading(patient.getBloodCholesterolReading());
	}

}
