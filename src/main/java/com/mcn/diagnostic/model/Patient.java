package com.mcn.diagnostic.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mcn.diagnostic.serialize.LocalDateDeserializer_LocaleGB;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Patient {

	@JsonProperty("id") 
	private String id;	
	
	@JsonProperty("first-name")
	private String firstName;
	
	@JsonProperty("last-name")
	private String lastName;
	
	@JsonProperty("birthdate")
	@JsonDeserialize(using = LocalDateDeserializer_LocaleGB.class) 
	private LocalDate birthDate; 
	
	@JsonProperty("blood-cholesterol-reading")
	private int bloodCholesterolReading;
	
	public Patient(String id, String name) {
		this.id = id;
		this.firstName = name;
	}
	
}
