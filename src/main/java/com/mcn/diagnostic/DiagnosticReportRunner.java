package com.mcn.diagnostic;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mcn.diagnostic.model.DiagnosticReport;

public class DiagnosticReportRunner {
	
	
	private static final String HOME = System.getProperty("user.home");
	private static final String JSON_FILE = HOME.concat("/patientdetails.json");

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		DiagnosticReportCreator drc = new DiagnosticReportCreator(new FileInputStream(JSON_FILE));

		Scanner scanner = new Scanner(System.in);
		String reportId = null;

		while (true) {
			try {
				System.out.print("Enter the patient ID (or enter 'x' to exit): ");
				reportId = scanner.next().trim();

				if (shouldExit(reportId)) {
					break;
				}

				DiagnosticReport report = drc.createDiagnosticReport(reportId);
				System.out.format("\n -- Diagnostic Report:\n%s\n", formatReport(report));
			} catch (IllegalArgumentException ex) {
				System.out.format("\n -- Could not find Patient for ID: %s\n\n", reportId);

			}
		}
		scanner.close();
		System.out.println("\nExiting...");
	}

	private static boolean shouldExit(String reportId) {
		return "x".equalsIgnoreCase(reportId);
	}

	private static String formatReport(DiagnosticReport report) {
		return "ID: ".concat(report.getId()).concat("\n") //
				.concat("Name: ").concat(report.getName()).concat("\n") //
				.concat("Age: ").concat("" + report.getAge()).concat("\n") //
				.concat("Blood cholesterol reading: ").concat("" + report.getCholesterolReading()).concat("\n") //
				.concat("Blood cholesterol level: ").concat(report.getCholesterolLevel().name()).concat("\n"); //
	}
}
